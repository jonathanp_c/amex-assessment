({
	init: function (cmp, event, helper) {
        cmp.set('v.columns', [
            {label: "Name", fieldName: "Name", type: 'text'},
            {label: "Type", fieldName: "Type__c", type: 'text'},
            {label: "Desciption", fieldName: "Description__c", type: 'text'},
            {label: "ID", fieldName:"Id", type: 'text'},

        ])
        
        cmp.set('v.columns2', [
            {label: "Name", fieldName: "Contract_Clause__r.name", type: 'text'},
            {label: "Type", fieldName: "Contract_Clause__r.Type__c", type: 'text'},
            {label: "Desciption", fieldName: "Contract_Clause__r.Description__c", type: 'text'},
			{label: "ID", fieldName:"Id", type: 'text'},
        ])
        
       	helper.getData(cmp, event);
		
		helper.getClauses(cmp, event);
        
    },
    
    updateSelected: function (cmp, event) {
        var selectedRows = event.getParam('selectedRows');
        alert(JSON.stringify (selectedRows));
        var selectedRowsID = []; 
        for( var i =0; i< selectedRows.length; i++){
            	selectedRowsID.push(selectedRows[i].id)
                //alert(selectedRows[i].id);
        }
        cmp.set('v.selectedRowsID', selectedRowsID);
        //alert(selectedRowsID);
    },
            
   
    
})